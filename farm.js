const moment = require('./scripts/moment');

describe('Grepolis villages farm with premium', function() {
	browser.waitForAngularEnabled(false);
	console.log("Start Time: " + moment().format("HH:mm:ss"));

	it('loggin to TEBY world', function() {
		element.all(by.className('world_name')).first().element(by.tagName('div')).click();
		browser.sleep(3000);
	});

	var click = 0;
	var count = 20;
	var sleepTime = (1000 * 60 * 5) + 5000;
	var timeDifference = 90;
	var newTime = sleepTime + (Math.floor((Math.random() * timeDifference)) * 1000);

	for(var i = 0; i < count; i++) {
		it('open villages window', function() {
			var premium = element.all(by.className('toolbar_button premium')).first().element(by.className('icon'));
			browser.actions().mouseMove(premium).perform();
			browser.sleep(500);

			element(by.name('farm_town_overview')).click();
			browser.sleep(1000);
		});

		it('just faaarrrrrmmmmm!!!', function() {
			element.all(by.className('gpwindow_content')).first().element(by.id('fto_claim_button')).click();
			console.log("\nTime: " + moment().format("HH:mm:ss"));
			console.log('Click: ', click);

			browser.sleep(1000);
			var tmp = element.all(by.className('gpwindow_content')).first();
			tmp.all(by.className('fto_town')).first().click();
			browser.sleep(1000);
			element.all(by.className('gpwindow_content')).first().element(by.id('fto_claim_button')).click();
			browser.sleep(1000);

			click++;
		});

		it('close villages window', function() {
			element(by.className('ui-dialog-titlebar-close')).click();
			console.log("Sleep Time: " + newTime);
			browser.sleep(newTime);
		});
	}

});