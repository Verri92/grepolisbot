exports.config = {
	directConnect: true,
	// seleniumAddress: 'http://localhost:4444/wd/hub',
	framework: 'jasmine',
	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 600000,
		isVerbose: true
	},
	specs: ['farm.js'],
	capabilites: {
		'browserName': 'chrome'
	},
	allScriptsTimeout: 600000,
	onPrepare: function() {
		browser.driver.get('https://pl.grepolis.com/');

		browser.driver.findElement(by.id('login_userid')).sendKeys('Verri92');
		browser.driver.findElement(by.id('login_password')).sendKeys('2fajnecycki');
		browser.driver.findElement(by.id('login_Login')).click();

		return browser.driver.wait(function() {
			return browser.driver.getCurrentUrl().then(function(url) {
				return /index/.test(url);
			});
		}, 10000);
	}
};